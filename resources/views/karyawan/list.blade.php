@extends('templates/index')
@section('content')


<section class="card">
    <div class="card-block">
        <div class=" mt-3 mb-3">
            <h3 class="text-center">Data Pegawai Cofnas</h3>
            <button type="button" class="tabledit-edit-button btn btn-sm btn-warning text-lg-left" data-toggle="modal" data-target=".TambahData">
                <span class=" glyphicon glyphicon-plus"></span>
            </button>
        </div>
        @if(Session::has('pesan-gagal'))
        <div class="alert alert-danger" role="alert">
            {{Session::get('pesan-gagal')}}
        </div>
        @endif

        @if(Session::has('pesan-berhasil'))
        <div class="alert alert-success" role="alert">
            {{Session::get('pesan-berhasil')}}
        </div>
        @endif
        <table id="table-edit" class="table table-bordered table-hover table-responsive-lg">
            <thead class="text-center">
                <tr>
                    <th width="1">No</th>
                    <th>Nama</th>
                    <th>Tanggal Lahir</th>
                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Departemen</th>
                    <th>HandPhone</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tr>
                @php $no = 1; @endphp
                <tbody class="text-center">
                    @forelse($karyawan as $row)
                    <tr id="1">
                        <td>{{ $no++ }}</td>
                        <td class="tabledit-view-mode">{{ $row->name }}</td>
                        <td class="table-icon-cell">{{ $row->tgl_lahir }}</td>
                        <td class="table-icon-cell">
                            <?php if ($row->jk == "P") {
                                echo 'Perempuan';
                            } elseif ($row->jk == "L") {
                                echo 'Laki-Laki';
                            }
                            ?>
                        </td>
                        <td class="table-icon-cell">{{ $row->agama }}</td>
                        <td class="table-icon-cell">{{ $row->departemen }}</td>
                        <td class="table-icon-cell">{{ $row->no_hp }}</td>
                        <td class="table-icon-cell">{{ $row->alamat }}</td>
                        <td style="white-space: nowrap; width: 1%;">
                            <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                <div class="btn-group btn-group-sm" style="float: none;">
                                    <button type="button" class="tabledit-edit-button btn btn-sm btn-warning ml-1" data-toggle="modal" data-target=".EditData-{{ $row->id }}">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button type="button" class="tabledit-delete-button btn btn-sm btn-danger" data-toggle="modal" data-target=".HapusData">
                                        <span class=" glyphicon glyphicon-trash"></span>
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td class="text-center text-danger font-italic fs-5" colspan="11">
                            <b> Data Pegawai Kosong, Silahkan Ditambahkan!</b>
                        </td>
                    </tr>
                    @endforelse
                </tbody>
        </table>
    </div>
</section>

<!-- Tambah Data Pegawai -->
<div class="modal fade TambahData" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title" id="myModalLabel">Tambah Data Pegawai</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('karyawan.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama Pegawai :</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Nama Pegawai">
                    </div>
                    <div class="mb-3">
                        <label for="tgl_lahir" class="form-label ">Tanggal Lahir :</label>
                        <input type="date" class="form-control " id="tgl_lahir" name="tgl_lahir">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect2">Jenis Kelamin</label>
                        <select name="jk" class="form-control">
                            <option disabled selected>Jenis Kelamin</option>
                            <option value="P">Perempuan</option>
                            <option value="L">Laki-Laki</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect2">Agama Pegawai</label>
                        <select name="agama" class="form-control">
                            <option disabled selected>Agama</option>
                            <option value="Katolik">Katolik</option>
                            <option value="Protestan">Protestan</option>
                            <option value="Islam">Islam</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Buddha">Buddha</option>
                            <option value="Khonghucu">Khonghucu</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="category" class="form-label">Departemen :</label>
                        <select name="departemen_id" id="departemen_id" class="form-control">
                            <option disabled selected>Pilih Departemen</option>
                            @foreach($departemen as $row)
                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="no_hp" class="form-label ">Nomor HP :</label>
                        <input type="text" class="form-control " id="no_hp" placeholder="Masukan Nomor hp" name="no_hp">
                    </div>
                    <div class="mb-3">
                        <label for="alamat" class="form-label ">Alamat :</label>
                        <input type="text" class="form-control " id="alamat" placeholder="Masukan Alamat" name="alamat">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-rounded btn-warning" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-rounded btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Edit Data Pegawai -->
@foreach($karyawan as $row)
<div class="modal fade EditData-{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title" id="myModalLabel">Ubah Data Pegawai</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('karyawan.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="id" class="form-label"></label>
                        <input type="hidden" class="form-control" id="id" name="id" value="{{ $row->id}}">
                    </div>
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama Pegawai :</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ $row->name}}">
                    </div>
                    <div class="mb-3">
                        <label for="tgl_lahir" class="form-label ">Tanggal Lahir :</label>
                        <input type="date" class="form-control " id="tgl_lahir" name="tgl_lahir" value="{{ $row->tgl_lahir}}">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect2">Jenis Kelamin</label>
                        <select name="jk" class="form-control">
                            <option value="{{ $row->jk }}">
                                <?php if ($row->jk == "P") {
                                    echo 'Perempuan';
                                } elseif ($row->jk == "L") {
                                    echo 'Laki-laki';
                                }
                                ?>
                            </option>
                            <option value="P">Perempuan</option>
                            <option value="L">Laki-Laki</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="agama pegawai">Agama Pegawai</label>
                        <select name="agama" class="form-control">
                            <option value="{{ $row->agama }}">{{ $row->agama }}</option>
                            <option value="Katolik">Katolik</option>
                            <option value="Protestan">Protestan</option>
                            <option value="Islam">Islam</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Buddha">Buddha</option>
                            <option value="Khonghucu">Khonghucu</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="departemen" class="form-label">Departemen :</label>
                        <select name="departemen_id" id="departemen_id" class="form-control">
                            <option value="{{ $row->departemen_id }}">{{ $row->departemen }}</option>
                            @foreach($departemen as $d)
                            <option value="{{ $d->id }}">{{ $d->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="no_hp" class="form-label ">Nomor HP :</label>
                        <input type="text" class="form-control " id="no_hp" name="no_hp" value="{{ $row->no_hp }}">
                    </div>
                    <div class="mb-3">
                        <label for="alamat" class="form-label ">Alamat :</label>
                        <input type="text" class="form-control " id="alamat" name="alamat" value="{{ $row->alamat }}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-rounded btn-warning" data-dismiss="modal">Batal</span></button>
                        <button type="submit" class="btn btn-rounded btn-primary">Simpan</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- Hapus Data Pegawai -->
<div class="modal HapusData" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title " id="myModalLabel">Anda Yakin Ingin Menghapus Data Pegawai ?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-rounded btn-warning" data-dismiss="modal">Batal</span></button>
                <a href="{{ route('karyawan.destroy',$row->id) }}" class="btn btn-rounded btn-danger">Hapus</a>
            </div>
        </div>
    </div>
</div>


@endforeach


@endsection