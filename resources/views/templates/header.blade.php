<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ $title }}</title>

    <link href="{{ asset('assets/img/favicon.144x144.png') }}" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="{{ asset('assets/img/favicon.114x114.png') }}" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="{{ asset('assets/img/favicon.72x72.png') }}" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="{{ asset('assets/img/favicon.57x57.png') }}" rel="apple-touch-icon" type="image/png">
    <link href="{{ asset('assets/img/favicon.png') }}" rel="icon" type="image/png">
    <link href="{{ asset('assets/img/favicon.ico') }}" rel="shortcut icon">


    <script src="{{ asset('assets/https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"') }}></script>
    <script src=" {{ asset('assets/https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>


    <link rel="stylesheet" href="{{ asset('assets/css/lib/lobipanel/lobipanel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/lobipanel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/jqueryui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/pages/widgets.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/lib/font-awesome/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">


    <link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap-sweetalert/sweetalert.css') }}">
</head>

<body class="horizontal-navigation">