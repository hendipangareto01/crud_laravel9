<script src="{{ asset('assets/js/lib/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/popper/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/tether/tether.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset ('assets/js/plugins.js') }}"></script>

<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
<script type="text/javascript" src="{{ asset('assets/js/lib/match-height/jquery.matchHeight.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/https://www.gstatic.com/charts/loader.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-sweetalert/sweetalert.min.js') }}"></script>

<script src="{{ asset('assets/js/app.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>

<!-- <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script> -->
<script src="{{ asset('assets/js/plugins.js') }}"></script>
<script>
    // table data
    $(function() {
        $('#example').DataTable();
    });
</script>
<script>
    $(document).ready(function() {
        $('.swal-btn-basic').click(function(e) {
            e.preventDefault();
            swal("Here's a message!");
        });

        $('.swal-btn-text').click(function(e) {
            e.preventDefault();
            swal({
                title: "Here's a message!",
                text: "It's pretty, isn't it?"
            });
        });

        $('.swal-btn-success').click(function(e) {
            e.preventDefault();
            swal({
                title: "Good job!",
                text: "You clicked the button!",
                type: "success",
                confirmButtonClass: "btn-success",
                confirmButtonText: "Success"
            });
        });

        $('.swal-btn-warning').click(function(e) {
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonClass: "btn-default",
                    confirmButtonClass: "btn-warning",
                    confirmButtonText: "Warning",
                    closeOnConfirm: false
                },
                function() {
                    swal({
                        title: "Deleted!",
                        text: "Your imaginary file has been deleted.",
                        type: "success",
                        confirmButtonClass: "btn-success"
                    });
                });
        });

        $('.swal-btn-cancel').click(function(e) {
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: "Deleted!",
                            text: "Your imaginary file has been deleted.",
                            type: "success",
                            confirmButtonClass: "btn-success"
                        });
                    } else {
                        swal({
                            title: "Cancelled",
                            text: "Your imaginary file is safe :)",
                            type: "error",
                            confirmButtonClass: "btn-danger"
                        });
                    }
                });
        });

        $('.swal-btn-custom-img').click(function(e) {
            e.preventDefault();
            swal({
                title: "Sweet!",
                text: "Here's a custom image.",
                confirmButtonClass: "btn-success",
                imageUrl: 'img/smile.png'
            });
        });

        $('.swal-btn-info').click(function(e) {
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this imaginary file!",
                type: "info",
                showCancelButton: true,
                cancelButtonClass: "btn-default",
                confirmButtonText: "Info",
                confirmButtonClass: "btn-primary"
            });
        });

        $('.swal-btn-input').click(function(e) {
            e.preventDefault();
            swal({
                title: "An input!",
                text: "Write something interesting:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                inputPlaceholder: "Write something"
            }, function(inputValue) {
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError("You need to write something!");
                    return false
                }
                swal("Nice!", "You wrote: " + inputValue, "success");
            });
        });
    });
</script>
</body>

</html>