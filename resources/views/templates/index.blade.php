@include('templates/header')
@include('templates/navbar')



<div class="page-content">
    <div class="container-fluid">

        @yield('content')

    </div>
</div>



@include('templates/footer')