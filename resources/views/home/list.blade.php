@extends('templates/index')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        <div class="box-typical box-typical-padding">
            <h4 class="with-border">Sweet Alert</h4>

            <div class="alert alert-success" role="alert">
                <p>
                    SweetAlert automatically centers itself on the page and looks great no matter if you're using a desktop computer, mobile or tablet. <br> It's even highly customizeable, as you can see below!
                </p>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="card mb-4">
                        <div class="card-header">
                            Basic message
                        </div>
                        <div class="card-block">
                            <p class="card-text">
                                <button class="btn btn-primary swal-btn-basic">Try Alert!</button>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4">
                        <div class="card-header">
                            Title with a text under
                        </div>
                        <div class="card-block">
                            <p class="card-text">
                                <button class="btn btn-primary swal-btn-text">Try Alert!</button>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4">
                        <div class="card-header">
                            Success message!
                        </div>
                        <div class="card-block">
                            <p class="card-text">
                                <button class="btn btn-primary swal-btn-success">Try Alert!</button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card mb-4">
                        <div class="card-header">
                            Warning message
                        </div>
                        <div class="card-block">
                            <p class="card-text">
                                <button class="btn btn-primary swal-btn-warning">Try Alert!</button>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4">
                        <div class="card-header">
                            Cancel message
                        </div>
                        <div class="card-block">
                            <p class="card-text">
                                <button class="btn btn-primary swal-btn-cancel">Try Alert!</button>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4">
                        <div class="card-header">
                            Info message
                        </div>
                        <div class="card-block">
                            <p class="card-text">
                                <button class="btn btn-primary swal-btn-info">Try Alert!</button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            Custom image message
                        </div>
                        <div class="card-block">
                            <p class="card-text">
                                <button class="btn btn-primary swal-btn-custom-img">Try Alert!</button>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            Input message
                        </div>
                        <div class="card-block">
                            <p class="card-text">
                                <button class="btn btn-primary swal-btn-input">Try Alert!</button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--.box-typical-->
    </div>
    <!--.container-fluid-->
</div>
<!--.page-content-->
<div class="container-fluid">
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">

                </div>
            </div>
        </div>
    </header>
    <section class="card">
        <div class="card-block">
            <div id="example_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="dataTables_length" id="example_length"><label>Show <select name="example_length" aria-controls="example" class="form-control form-control-sm">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select> entries</label></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div id="example_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example"></label></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example" class="display table table-striped table-bordered dataTable" role="grid" aria-describedby="example_info" style="width: 100%;" width="100%" cellspacing="0">
                            <thead>
                                <tr role="row">
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 138.317px;" aria-label="Name: activate to sort column ascending">Name</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 233.517px;" aria-label="Position: activate to sort column ascending">Position</th>
                                    <th class="sorting_desc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 98.6167px;" aria-label="Office: activate to sort column ascending" aria-sort="descending">Office</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 46.5333px;" aria-label="Age: activate to sort column ascending">Age</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 96.8167px;" aria-label="Start date: activate to sort column ascending">Start date</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 76.2px;" aria-label="Salary: activate to sort column ascending">Salary</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" class="odd">
                                    <td class="">Garrett Winters</td>
                                    <td>Accountant</td>
                                    <td class="sorting_1">Tokyo</td>
                                    <td>63</td>
                                    <td>2011/07/25</td>
                                    <td>$170,750</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 45 entries (filtered from 57 total entries)</div>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                            <ul class="pagination">
                                <li class="paginate_button page-item previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
                                <li class="paginate_button page-item active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                                <li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
                                <li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
                                <li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="4" tabindex="0" class="page-link">4</a></li>
                                <li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="5" tabindex="0" class="page-link">5</a></li>
                                <li class="paginate_button page-item next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="6" tabindex="0" class="page-link">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection