@extends('templates/index')
@section('content')

<section class="card">
    <div class="card-block">
        <div class=" mt-3 mb-3">
            <h3 class="text-center">Data Departemen Cofnas</h3>
            <button type="button" class="tabledit-edit-button btn btn-sm btn-warning text-lg-left" data-toggle="modal" data-target=".TambahData">
                <span class=" glyphicon glyphicon-plus"></span>
            </button>
        </div>
        @if(Session::has('pesan-gagal'))
        <div class="alert alert-danger" role="alert">
            {{Session::get('pesan-gagal')}}
        </div>
        @endif

        @if(Session::has('pesan-berhasil'))
        <div class="alert alert-success" role="alert">
            {{Session::get('pesan-berhasil')}}
        </div>
        @endif
        <table id="table-edit" class="table table-bordered table-hover table-responsive-lg">
            <thead class="text-center">
                <tr>
                    <th width="1">No</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tr>
                @php $no = 1; @endphp
                <tbody class="text-center">
                    @forelse($departemen as $row)
                    <tr id="1">
                        <td>{{ $no++ }}</td>
                        <td class="tabledit-view-mode">{{ $row->name }}</td>
                        <td style="white-space: nowrap; width: 1%;">
                            <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                                <div class="btn-group btn-group-sm" style="float: none;">
                                    <button type="button" class="tabledit-edit-button btn btn-sm btn-warning ml-1" data-toggle="modal" data-target=".EditData-{{ $row->id }}">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                    <button type="button" class="tabledit-delete-button btn btn-sm btn-danger" data-toggle="modal" data-target=".HapusData">
                                        <span class=" glyphicon glyphicon-trash"></span>
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td class="text-center text-danger font-italic fs-5" colspan="11">
                            <b> Data Departemen Kosong, Silahkan Ditambahkan!</b>
                        </td>
                    </tr>
                    @endforelse
                </tbody>
        </table>
    </div>
</section>


<!-- Tambah Data Departemen -->
<div class="modal TambahData" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title " id="myModalLabel">Tambah Data Departemen</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('departemen.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama Departemen :</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Nama Pegawai">
                    </div>
                    <div class="form-group">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-rounded btn-warning" data-bs-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-rounded btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Ubah Data Departemen -->
@foreach($departemen as $row)
<div class="modal EditData-{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title " id="myModalLabel">Ubah Data Departemen</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('departemen.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="id" class="form-label"></label>
                        <input type="hidden" class="form-control" id="id" name="id" value="{{ $row->id}}">
                    </div>
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama Departemen :</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ $row->name}}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-rounded btn-warning" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-rounded btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Hapus Data Departemen -->
<div class="modal HapusData" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title " id="myModalLabel">Anda Yakin Ingin Menghapus Data Departemen ?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-rounded btn-warning" data-dismiss="modal">Batal</span></button>
                <a href="{{ route('departemen.destroy',$row->id) }}" class="btn btn-rounded btn-danger">Hapus</a>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection