<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [App\Http\Controllers\HomeController::class, 'home'])->name('halaman.home');


Route::get('/karyawan', [App\Http\Controllers\KaryawanController::class, 'index'])->name('karyawan.index');
Route::prefix('karyawan')->group(function () {
    Route::get('create', [App\Http\Controllers\KaryawanController::class, 'create'])->name('karyawan.create');
    Route::post('store', [App\Http\Controllers\KaryawanController::class, 'store'])->name('karyawan.store');

    Route::get('edit/{id}', [App\Http\Controllers\KaryawanController::class, 'edit'])->name('karyawan.edit');
    Route::post('update', [App\Http\Controllers\KaryawanController::class, 'update'])->name('karyawan.update');

    Route::get('destroy/{id}', [App\Http\Controllers\KaryawanController::class, 'destroy'])->name('karyawan.destroy');
});



// ================DEPARTEMEN========>
Route::get('/index', [App\Http\Controllers\DepartemenController::class, 'index'])->name('departemen.index');
Route::prefix('departemen')->group(function () {

    Route::get('create', [App\Http\Controllers\DepartemenController::class, 'create'])->name('departemen.create');
    Route::post('store', [App\Http\Controllers\DepartemenController::class, 'store'])->name('departemen.store');

    Route::get('edit/{id}', [App\Http\Controllers\DepartemenController::class, 'edit'])->name('departemen.edit');
    Route::post('update', [App\Http\Controllers\DepartemenController::class, 'update'])->name('departemen.update');

    Route::get('destroy/{id}', [App\Http\Controllers\DepartemenController::class, 'destroy'])->name('departemen.destroy');
});
