<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        $title = 'Halaman Home';
        return view('home.list', compact('title'));
    }
}
