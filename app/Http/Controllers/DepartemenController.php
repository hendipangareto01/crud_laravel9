<?php

namespace App\Http\Controllers;

use App\Models\Departemen;
use Illuminate\Http\Request;

class DepartemenController extends Controller
{
    public function index()
    {
        $title = 'Halaman Departemen';
        $departemen = Departemen::latest()->get();
        return view('departemen.list', compact('departemen', 'title'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:50',
        ]);

        // dd($validateData);

        try {
            Departemen::create($validateData);
            return redirect(route('departemen.index'))->with('pesan-berhasil', 'Anda berhasil menambah data');
        } catch (\Exception $e) {
            return redirect(route('departemen.index'))->with('pesan-gagal', 'Anda gagal menambah data');
        }
    }


    public function show(Departemen $departemen)
    {
        //
    }

    public function edit(Departemen $departemen)
    {
        //
    }

    public function update(Request $request, Departemen $departemen)
    {
        $departemen = Departemen::findOrFail($request->id);
        $departemen->name = $request->name;


        // dd($departemen);
        try {
            $departemen->update();
            return redirect(route('departemen.index'))->with('pesan-berhasil', 'Anda berhasil mengubah data');
        } catch (\Exception $e) {
            return redirect(route('departemen.index'))->with('pesan-gagal', 'Anda gagal mengubah data');
        }
    }


    public function destroy($id)
    {
        $departemen = Departemen::findOrFail($id);
        try {
            $departemen->delete();
            return redirect(route('departemen.index'))->with('pesan-berhasil', 'Anda berhasil menghapus data');
        } catch (\Exception $e) {

            return redirect(route('departemen.index'))->with('pesan-gagal', 'Anda gagal menghapus data');
        }
    }
}
