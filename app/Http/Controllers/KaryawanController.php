<?php

namespace App\Http\Controllers;

use App\Models\Departemen;
use App\Models\Karyawan;
use Illuminate\Http\Request;

class KaryawanController extends Controller
{

    public function index()
    {
        $title = 'Data Pegawai';
        $karyawan = Karyawan::select("karyawan.*", 'departemen.name as departemen')
            ->join('departemen', 'departemen.id', '=', 'karyawan.departemen_id')->orderBy('karyawan.id')->get();
        $departemen = Departemen::latest()->get();
        return view('karyawan.list', compact('title', 'karyawan', 'departemen'));
    }

    public function create()
    {
        $karyawan = Karyawan::all();
    }


    public function store(Request $request)
    {
        $validateData = $request->validate([
            'departemen_id' => 'required',
            'name' => 'required|max:25',
            'tgl_lahir' => 'required',
            'jk' => 'required',
            'agama' => 'required',
            'no_hp' => 'required|max:12',
            'alamat' => 'required'
        ]);
        // dd($validateData);
        try {
            Karyawan::create($validateData);
            return redirect(route('karyawan.index'))->with('pesan-berhasil', 'Anda berhasil menambah data');
        } catch (\Exception $e) {
            return redirect(route('karyawan.index'))->with('pesan-gagal', 'Anda gagal menambah data');
        }
    }


    public function edit($id)
    {
        $karyawan = Karyawan::findOrFail($id);
    }

    public function update(Request $request)
    {
        $karyawan = Karyawan::findOrFail($request->id);
        $karyawan->departemen_id = $request->departemen_id;
        $karyawan->name = $request->name;
        $karyawan->tgl_lahir = $request->tgl_lahir;
        $karyawan->jk = $request->jk;
        $karyawan->agama = $request->agama;
        $karyawan->no_hp = $request->no_hp;
        $karyawan->alamat = $request->alamat;


        // dd($karyawan);
        try {
            $karyawan->update();
            return redirect(route('karyawan.index'))->with('pesan-berhasil', 'Anda berhasil mengubah data');
        } catch (\Exception $e) {
            return redirect(route('karyawan.index'))->with('pesan-gagal', 'Anda gagal mengubah data');
        }
    }


    public function destroy($id)
    {
        $karyawan = Karyawan::findOrFail($id);
        try {
            $karyawan->delete();
            return redirect(route('karyawan.index'))->with('pesan-berhasil', 'Anda berhasil menghapus data');
        } catch (\Exception $e) {

            return redirect(route('karyawan.index'))->with('pesan-gagal', 'Anda gagal menghapus data');
        }
    }
}
