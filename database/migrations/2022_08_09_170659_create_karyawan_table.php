<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->id();
            $table->foreignId('departemen_id');
            $table->string('name');
            $table->date('tgl_lahir');
            $table->enum('jk', ['P', 'L'])->nullable();
            $table->enum('agama', ['Katolik', 'Protestan', 'Islam', 'Hindu', 'Buddha', 'Khonghucu'])->nullable();
            $table->string('no_hp');
            $table->text('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
