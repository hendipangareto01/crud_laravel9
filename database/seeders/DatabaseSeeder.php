<?php

namespace Database\Seeders;

use App\Models\Karyawan;
use App\Models\Departemen;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Karyawan::create([
            'departemen_id' => '1',
            'name' => 'Hendi',
            'tgl_lahir' => '2001-01-24',
            'jk' => 'L',
            'agama' => 'Katolik',
            'no_hp' => '082144146795',
            'alamat' => 'Jogja',

        ]);

        Departemen::create([
            'id' => '1',
            'name' => 'Departemen IT'
        ]);
    }
}
